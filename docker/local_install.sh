#!/bin/bash


CURRENT_PATH=$(dirname $(readlink -f $0))
WORK_DIR=$(cd "${CURRENT_PATH}/../../"; pwd)
BUILD_ARGS=""
PATCH="" # 是否在cantian中创建元数据
BUILD_TYPE="release"
USER="cantiandba"

function prepare() {
  echo "Prepare env start."
  yum install -y libaio-devel openssl openssl-devel \
  ndctl-devel ncurses ncurses-devel libtirpc-devel \
  expect ant bison iputils iproute wget\
  libtirpc-devel make gcc gcc-c++ gdb gdb-gdbserver\
  python3 python3-devel git net-tools cmake automake\
  byacc libtool maven --nogpgcheck
  mkdir /tools
  git clone https://gitee.com/cantian-repo/dockerbuild.git
  cd dockerbuild/third_party/ || exit 1
  cat boost_1_73_0a* > boost_1_73_0.tar.gz
  rm -rf boost_1_73_0a*
  mv ./* /tools
  cd /tools || exit 1
  rm -rf dockerbuild
  echo "Prepare boost."
  unzip boost_1_73_0.tar.gz
  tar -zxvf rpcsvc-proto-1.4.tar.gz
  tar -zxf doxygen-1.9.2.src.tar.gz
  echo "make install doxygen."
  cd doxygen-1.9.2 && mkdir build
  cd build && cmake -G "Unix Makefiles" ..
  make
  cd ../.. && rm -rf doxygen-1.9.2
  echo "make install doxygen success."
  echo "Prepare rpcsvc."
  cd rpcsvc-proto-1.4 || exit 1
  ./configure && make && make install
  cd - || exit 1
  rm -rf rpcsvc-proto-1.4
  echo "Prepare rpcsvc success."
  echo "Prepare env success."
}

function mysql_patch() {
    cd "${WORK_DIR}"/cantian-connector-mysql/mysql-source || exit 1
    patch --ignore-whitespace -p1 < mysql-scripts-meta.patch
    patch --ignore-whitespace -p1 < mysql-test-meta.patch
    patch --ignore-whitespace -p1 < mysql-source-code-meta.patch
}

function cantian_patch() {
    escaped_variable=$(echo "${WORK_DIR}" | sed 's/\//\\\//g')
    sed -i "s/\/home\/regress\/CantianKernel/${escaped_variable}\/cantian/g" ${WORK_DIR}/cantian/pkg/install/install.py
    sed -i "s/\/home\/regress/${escaped_variable}/g" ${WORK_DIR}/cantian/pkg/install/Common.py
    sed -i "s/\/home\/regress/${escaped_variable}/g" ${WORK_DIR}/cantian/pkg/install/funclib.py
    sed -i "s/192.168.86.1/127.0.0.1/g" ${WORK_DIR}/cantian/pkg/install/funclib.py
}

function build_cantian() {
    cantian_patch
    export local_build=true
    cd ${WORK_DIR}/cantian/build || exit 1
    sh Makefile.sh package-${BUILD_TYPE} ${BUILD_ARGS}
    if [[ $? -ne 0  ]]; then
        echo "build_cantian failed."
        exit 1
    fi
}

function build_mysql() {
    if [[ x"${PATCH}" == x"true" ]]; then
        mysql_patch
    fi
    export local_build=true
    cd ${WORK_DIR}/cantian/build || exit 1
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${WORK_DIR}/cantian-connector-mysql/mysql-source/bld_debug/plugin_output_directory
    sh Makefile.sh mysql_${BUILD_TYPE} ${BUILD_ARGS}
    if [[ $? -ne 0  ]]; then
        echo "build_mysql failed."
        exit 1
    fi
    sh Makefile.sh mysql_package_node0
}

function compile() {
    if [[  ! -f ${WORK_DIR}/mysql-8.0.26.tar.gz ]]; then
        echo "wget mysql 8.0.26 code."
        wget --no-check-certificate https://github.com/mysql/mysql-server/archive/refs/tags/mysql-8.0.26.tar.gz
        mv mysql-8.0.26.tar.gz ${WORK_DIR}/
    fi
    cd ${WORK_DIR} || exit 1
    if [[ ! -f ${WORK_DIR}/mysql-8.0.26.tar.gz ]]; then
        echo "mysql 8.0.26 code not exist."
        exit 1
    fi
    if [[ -d cantian-connector-mysql/mysql-source ]]; then
        rm -rf cantian-connector-mysql/mysql-source
    fi # 删除之前编译的mysql源码
    tar -zxf mysql-8.0.26.tar.gz
    mv mysql-server-mysql-8.0.26 cantian-connector-mysql/mysql-source
    mkdir -p cantian_data
    cp -arf cantian-connector-mysql/storage cantian-connector-mysql/mysql-source/
    cp -arf cantian-connector-mysql/mysql-test cantian-connector-mysql/mysql-source/
    cp -arf cantian-connector-mysql/mysql-test/* cantian-connector-mysql/mysql-source/
    # 注释maintainer.cmake文件中两行
    sed -i '/STRING_APPEND(MY_C_WARNING_FLAGS   " -Werror")/s/^/#/' cantian-connector-mysql/mysql-source/cmake/maintainer.cmake
    sed -i '/STRING_APPEND(MY_CXX_WARNING_FLAGS " -Werror")/s/^/#/' cantian-connector-mysql/mysql-source/cmake/maintainer.cmake
    build_cantian
    build_mysql
}

function clean() {
    kill -9 $(pidof mysqld) > /dev/null 2>&1
    kill -9 $(pidof cantiand) > /dev/null 2>&1
    kill -9 $(pidof cms) > /dev/null 2>&1
    rm -rf ${WORK_DIR}/cantian_data/* /home/${USER}/install /home/${USER}/data /data/data/*
    sed -i "/${USER}/d" /home/${USER}/.bashrc
}

function install() {
    id "${USER}"
    if [[ $? -ne 0 ]]; then
        echo "add user ${USER}."
        useradd -m -s /bin/bash ${USER}
        echo "${USER}:${USER}" | chpasswd
    fi
    touch /.dockerenv
    clean
    cd "${WORK_DIR}"/cantian/Cantian-DATABASE-CENTOS-64bit || exit 1
    mkdir -p /home/${USER}/logs
    run_mode=cantiand_in_cluster
    mysql_cnf=""
    if [[ -f "${WORK_DIR}"/single_flag ]]; then
        mysql_cnf="-m ${WORK_DIR}/cantian-connector-mysql/scripts/my.cnf"
        run_mode=cantiand_with_mysql_in_cluster
    fi

    python3 install.py -U ${USER}:${USER} -R /home/${USER}/install \
    -D /home/${USER}/data -l /home/${USER}/logs/install.log \
    -M ${run_mode} ${mysql_cnf} -Z _LOG_LEVEL=255 -N 0 -W 192.168.0.1 -g \
    withoutroot -d -c -Z _SYS_PASSWORD=Huawei@123 -Z SESSIONS=1000
    if [[ $? -ne 0  ]]; then
        echo "install cantian failed."
        exit 1
    fi
    if [[ ! -f "${WORK_DIR}"/single_flag ]]; then
        python3 install.py -U cantiandba:cantiandba -l /home/cantiandba/logs/install.log -d -M mysqld -m "${WORK_DIR}"/cantian-connector-mysql/scripts/my.cnf
        if [[ $? -ne 0 ]]; then
            echo "start mysql failed."
            exit 1
        fi
    fi
}

function usage() {
    echo 'Usage: sh local_install.sh compile [OPTION]'
    echo 'Options:'
    echo '  -s, --single                  Build single process.'
    echo '  -b, --build_type=<type>       Build type, default is release.'
    echo '  -p, --patch                   Patch mysql source code.'
    echo '  -u, --user=<user>             User name, default is cantiandba.'
    echo '  -h, --help                    Display thishelp and exit.'
}

function parse_params()
{
    ARGS=$(getopt -o spb:u: --long single,build_type:,patch,user:, -n "$0" -- "$@")
    if [ $? != 0 ]; then
        echo "Terminating..."
        exit 1
    fi
    eval set -- "${ARGS}"
    while true
    do
        case "$1" in
            -s | --single)
                BUILD_ARGS="${BUILD_ARGS} CANTIAN_READ_WRITE=1 no_shm=1"
                touch "${WORK_DIR}"/single_flag
                shift
                ;;
            -b | --build_type)
                BUILD_TYPE=$2
                shift 2
                ;;
            -p | --patch)
                PATCH=true
                shift
                ;;
            -u | --user)
                USER=$2
                shift 2
                ;;
            --)
                shift
                break
                ;;
            -h)
                usage
                exit 1
                ;;
        esac
    done
}

function help() {
    echo 'Usage: sh local_install.sh [OPTION]'
    echo 'Options:'
    echo '  prepare                       Prepare compile and install dependencies.'
    echo '  compile                       Compile cantian and mysql.'
    echo '  install                       Install and start cantian and mysql.'
}

function main()
{
    mode=$1
    shift
    parse_params "$@"
    case $mode in
        prepare)
            prepare
            exit 0
            ;;
        compile)
            compile
            exit 0
            ;;
        install)
            install
            exit 0
            ;;
        clean)
            clean
            exit 0
            ;;
        *)
            help
            exit 1
            ;;
    esac
}

main "$@"